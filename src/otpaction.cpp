/*
This file is part of Trabucco.

Trabucco is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Trabucco is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Trabucco.  If not, see <http://www.gnu.org/licenses/>.

Copyright (C) 2023  Salvo "LtWorf" Tomaselli
*/

#include <QDir>
#include <QDirIterator>
#include <QFileInfo>
#include <QProcess>
#include <QSettings>

#include "iconfinder.h"
#include "otpaction.h"

static QString separator;

OtpAction::OtpAction(QString script, QString prefix, QObject *parent) : Action(parent)
{
    this->script = script;
    this->name = prefix + separator + script;
}

QString OtpAction::getIcon() {
    return this->cached_icon_path;
}

void OtpAction::runAction() {
    QStringList arguments;
    arguments << "otp";
    arguments << "-c";
    arguments << script;
    QProcess proc;
    proc.setProgram("pass");
    proc.setArguments(arguments);
    proc.startDetached();
}

void OtpAction::scanAndLoad(BTree* tree, QObject* parent, QString base, QString icon, QStringList prefixes, unsigned int lchop) {
    QDirIterator i(base, QDirIterator::FollowSymlinks);
    while (i.hasNext()) {
        QFileInfo file(i.next());

        if (file.fileName().startsWith("."))
            continue;
        if (file.isDir() && file.isExecutable()) {
            OtpAction::scanAndLoad(tree, parent, file.absoluteFilePath(), icon, prefixes, lchop);
        }
        if (!file.fileName().endsWith(".gpg"))
            continue;

        for (int i = 0; i < prefixes.size(); i++) {
            QString name = file.absoluteFilePath();
            // Remove extension
            name.chop(4);
            // Remove initial path
            name.remove(0, lchop);

            OtpAction* pass_action = new OtpAction(name, prefixes.at(i), parent);
            pass_action->cached_icon_path = icon;
            tree->add(pass_action);
        }
    }
}

void OtpAction::LoadOtpActions(BTree* tree, QObject* parent) {
    if (!OtpAction::isPassOtpInstalled()) {
        return;
    }
    QStringList* dirs = OtpAction::GetPaths();
    QString dir = dirs->at(0);

    QSettings settings;
    separator = settings.value("OtpAction/separator", " ").toString();

    QString icon = IconFinder::FindIcon("password-copy");
    QStringList prefixes = settings.value("OtpAction/prefixes", "otp").toStringList();

    OtpAction::scanAndLoad(tree, parent, dir, icon, prefixes, dir.size());
}

QStringList* OtpAction::GetPaths() {
    static QStringList* result = NULL;
    if (result)
        return result;

    result = new QStringList();
    result->append(QDir::homePath() + "/.password-store/");
    return result;
}

bool OtpAction::isPassOtpInstalled() {
    QFile pass("/usr/share/man/man1/pass-otp.1.gz");
    return pass.exists();
}
